var express = require('express');
var app = express();
var routes = require('./routes');
var bodyParser = require('body-parser')
var morgan = require('morgan')


app.use(morgan('dev'))
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())

app.use(routes)
module.exports = app