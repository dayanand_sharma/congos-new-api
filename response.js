
module.exports = {

    response: (req, res, error, result,errmsg) => {
        // console.log('result',result)
        console.log('error', error)
        if (error) {
            res.json({ "error": error })
        }
        else {
            if (result.rowCount <= 0) {
                res.json({ "msg": errmsg==undefined?"No Data Found":errmsg , "data":result.rows})
            }
            else { res.json({ "msg":"success","data": result.rows }) }

        }
    },

}